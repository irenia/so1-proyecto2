module consumer

go 1.13

require (
	github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0
)
