module producer

go 1.14

require (
	github.com/Shopify/sarama v1.29.0
	github.com/confluentinc/confluent-kafka-go v1.7.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/segmentio/kafka-go v0.4.16
	github.com/strimzi/client-examples/go/sarama v0.0.0-20210524153240-6f9a2860c0d6
	gopkg.in/confluentinc/confluent-kafka-go.v1 v1.7.0
)
