const fs = require('fs'); // Este modulo viene cargado directamente en nodejs, sirve para trabajar con archivos

// Aca es donde se lee el modulo kernel que cargamos!
// Este metodo va a leer la carpeta /proc/timestamps y convierte sus datos a un string.
const getTop = () => (fs.readFileSync('/elements/procs/procesoxtop', 'utf8')).toString(); 

// Este metodo sirve para devolver la informacion que hay en el modulo
// En caso la lectura no pueda ser efectuada, devolvemos un error!
// Esto es util ya que el server entocnces no se caera cada vez que requiramos la lectura y no tengamos bien el modulo
const safeGetTop = () => {

    // Intentamos hacer la lectura del archivo /proc/procesoxtop
    try {
        // Si todo es correcto, lo devolveremos
        return getTop();
    }
    // Deconstruimos el objeto error en el catch, y obtenemos unicamente su valor
    catch ({ message }) {
        // En dado caso haya un error, devolveremos el error.
        return `No se pudo leer el modulo de lista de procesos. ${message}`;
    }
}

// Exportamos el metodo que queremos utilizar, en este caso le renombramos a getTop
module.exports = { getTop: safeGetTop };