const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();
app.use(cors());
app.set('view engine', 'ejs');

//app.use(express.urlencoded({ extended: false }));

// IMPORTANTE! Le decimos a express que necesitamos trabajar con intercambio de datos JSON
app.use(express.json({ extended: true }))

// Connect to MongoDB
mongoose
  .connect(
    'mongodb://mongo:27017/docker-node-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

const Item = require('./models/Item'); // modelo de la collection en la bd en mongo

// Tabla de datos recopilados (ordenados con el último primero) con la capacidad de filtrarse por la ruta
app.get('/mensajes_real', async (req, res) => {
  await Item.find().sort({ _id: -1 }).then(items => res.json(items));
});

app.get('/items', async (req, res) => {
  await Item.find().sort({ _id: -1 }).then(items => res.json(items));
});

app.get('/mapa', async (req, res) => {
  await Item.aggregate([{ $group:{  _id:"$location",  confirmed:{$sum:1}, departamento:{$push:{contador:{$sum:1},genero:"$gender"}} } }] ).then(items => res.json(items));
});

// api para guardar los mensajes enviados de los microservicios (gRPC, NATS, RABBITMQ, GooglePubSub)
app.post('/', async (req, res) => {
  const newItem = new Item({
    name: req.body.name,
    location: req.body.location,
    gender: req.body.gender,
    age: req.body.age,
    vaccine_type: req.body.vaccine_type,
    way: req.body.way
  });
  console.log(newItem);
  await newItem.save().then(item => res.json("Mensaje insertado"));
});


const port = 3000;

app.listen(port, () => console.log('Server running...'));
