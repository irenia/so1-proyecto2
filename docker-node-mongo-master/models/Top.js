const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TopSchema = new Schema({
    dato: {
      type: String,
      required: true
    }
  });

  module.exports = Top = mongoose.model('top', TopSchema);