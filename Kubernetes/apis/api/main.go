package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func goGetVariableEnv(keyCode string) string {
	err := godotenv.Load(".env") //leo el archivo ".env"
	if err != nil {              //si es que hay error al leer el archivo
		log.Fatalf("No se pudueron leer las variables de entorno :(")
	}
	return os.Getenv(keyCode) //devuelvo la variable de entrono solicitada
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func indexRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "API recibio el dato")
	//Parsing body
	var body map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&body)
	failOnError(err, "Parsing JSON")
	body["way"] = "api"

	fmt.Print("Datos obtenidos en la API: ")
	fmt.Println(body)
}

func main() {
	http_port := ":" + goGetVariableEnv("PORT")                    //Obtengo el puerto al cual se conecta desde el ".env"
	fmt.Println("Server iniciado en http://localhost" + http_port) //Mensaje de inicializacion

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", indexRoute)
	if err := http.ListenAndServe(http_port, router); err != nil {
		log.Fatal(err)
	}
}
