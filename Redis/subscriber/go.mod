module subscriber

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.8.2
	github.com/gomodule/redigo v1.8.4 // indirect
)
