package main

import (
	"context"
	"encoding/json"

	"time"

	"fmt"
	"log"
	"net/http"
	"github.com/gorilla/mux"

	"github.com/go-redis/redis/v8"
)

func main() {
	http_port := ":7000"
	//Obligando que ese escriba la URL correcta
	router := mux.NewRouter().StrictSlash(true)
	
	//Definciendo la ruta, URL principal
	router.HandleFunc("/", locus)
	
	//Definciendo la ruta, URL principal
	router.HandleFunc("/salud", Salud)
	
		log.Println("Hello redis pub, your works on port 7000")
	
		//Levantamos el server
		//Por si existe un error
		if err := http.ListenAndServe(http_port, router); err != nil {
			log.Println("Error de conexion")
		}
	

}

func Salud(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK")
}

// esta funcion sirve para poder mandar peticiones a angular
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Println("%s: %s", msg, err)
	}
}

func locus( w http.ResponseWriter, r *http.Request){
	enableCors(&w) // habilitamos cors
	//obtenemos la data
	var body map[string]interface{}
	err3 := json.NewDecoder(r.Body).Decode(&body)
	failOnError(err3, "Parsing JSON")

	body["Way"] = "Redis"


	

	dataaux, err2 := json.Marshal(body)

	if err2 != nil {
		log.Println(err2)
		return
	}

	fmt.Println(string(dataaux))

		// Create a new Redis Client
		redisClient := redis.NewClient(&redis.Options{
			Addr:     "35.223.218.131:6379",  // We connect to host redis, thats what the hostname of the redis service is set to in the docker-compose
			Password: "", // The password IF set in the redis Config file
			DB:       0,
		})
		// Ping the Redis server and check if any errors occured
		err := redisClient.Ping(context.Background()).Err()
		if err != nil {
			// Sleep for 3 seconds and wait for Redis to initialize
			time.Sleep(3 * time.Second)
			err := redisClient.Ping(context.Background()).Err()
			if err != nil {
				panic(err)
			}
		}
		// Generate a new background context that  we will use
		ctx := context.Background()
		// Loop and randomly generate users on a random timer
		
			// Publish a generated user to the new_users channel
			err1 := redisClient.Publish(ctx, "new_users", string(dataaux)).Err()
			if err1 != nil {
				panic(err1)
			}
			w.Header().Set("Content-Type", "application/json")
			fmt.Fprintf(w, string(dataaux))


			
		


			
}


