package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"github.com/gorilla/mux"

	grpcapi "github.com/MarcosAlberto21/comunicacionClienteServidorGRPCwhitgolang"
	"google.golang.org/grpc"
)

type datos struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	gender		 string `json:gender`
	Age          int    `json:"age"`
	Vaccine_type string `json:"vaccine_type"`
}

//funcion que realizara la peticion de datos proveniente de locus o 
//del consumo de nuestro endpoint
func locus(w http.ResponseWriter, r *http.Request) {
	enableCors(&w) // habilitamos cors

	var body map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&body)
	failOnError(err, "Parsing JSON")

	body["way"] = "gRPC"

	fmt.Println(body)

	dataaux, err := json.Marshal(body)

	if err != nil {
		fmt.Fprintf(w, "%v", err)
	}

	// aqui creo la coneccion con grpc

	respuesta := client(dataaux)

	if respuesta != nil {
		fmt.Fprintf(w, "%v", respuesta)
	}
	if respuesta == nil {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, string(dataaux))
	}

	//log.Fatalf("error while calling gRPC: %v", err)

}

func server(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)

}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Println(msg)
	}
}

func client(data []byte) (respuesta error) {
	log.Println("Contruyendo solicitud... . . .")
	if data == nil || string(data) == "" {

		return
	}
	hostname := os.Getenv("SVC_HOST_NAME")
	log.Println(hostname)
	if len(hostname) <= 0 {
		//hostname = "localhost"
		hostname = "servidor"
	}

	port := os.Getenv("SVC_PORT")

	if len(port) <= 0 {
		port = "7080"
	}

	cc, err := grpc.Dial(hostname+":"+port, grpc.WithInsecure())

	if err != nil {
		respuesta = err
		return
		//log.Fatalf("could not connect: %v", err)
	}
	defer cc.Close()

	c := grpcapi.NewGrpcServiceClient(cc)

	log.Println(string(data))
	respuesta = callService(c, data)
	return

}

func callService(c grpcapi.GrpcServiceClient, data []byte) (respuesta error) {
	fmt.Println("Realizando peticion al server... . . .")
	req := &grpcapi.GrpcRequest{
		Input: string(data),
	}
	res, err := c.GrpcService(context.Background(), req)
	if err != nil {
		fmt.Printf("error while calling gRPC: %v", err)
	}
	err = nil
	//muestra en consola los datos
	log.Printf("Response from Service: %v", res.Response)
	return
}

func main() {
http_port := ":7000"
//Obligando que ese escriba la URL correcta
router := mux.NewRouter().StrictSlash(true)

//Definciendo la ruta, URL principal
router.HandleFunc("/", locus)

//Definciendo la ruta, URL principal
router.HandleFunc("/salud", Salud)

	log.Println("Hello gRPC pub, your works on port 7000")

	//Levantamos el server
	//Por si existe un error
	if err := http.ListenAndServe(http_port, router); err != nil {
		log.Fatal(err)
	}

}


func Salud(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK")
}

// esta funcion sirve para poder mandar peticiones a angular
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}
