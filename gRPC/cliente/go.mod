module cliente

go 1.12

require (
	github.com/MarcosAlberto21/comunicacionClienteServidorGRPCwhitgolang v0.0.0-20210312144917-42c41cad8d35
	github.com/gorilla/mux v1.8.0 // indirect
	google.golang.org/grpc v1.36.0
)
