module servidor

go 1.12

require (
	github.com/MarcosAlberto21/comunicacionClienteServidorGRPCwhitgolang v0.0.0-20210312144917-42c41cad8d35
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gomodule/redigo v1.8.4 // indirect
	google.golang.org/grpc v1.36.0
)
