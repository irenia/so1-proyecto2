import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import RealReporte6 from '../../../components/MyComponents/Reporte6';
import SelectCountry from '../../../components/MyComponents/SelectCountry';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import './style.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: '1%',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardAnimation: {
    'animation': 'appear 500ms ease-out forwards'
  },
}));

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  },
  redBtn: {
    color: '#f44336',
    'border-color': '#f44336'
  },
  blueBtn: {
    color: '#7986cb',
    'border-color': '#7986cb'
  },
  blackBtn: {
    color: '#000000',
    'border-color': '#000000'
  },
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
});

function Reporte6(props) {
  const title = brand.name + ' - Blank Page';
  const description = brand.desc;
  const { classes } = props;
  const classes_ = useStyles();

  return (
    <div>
      <Helmet>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      <PapperBlock title="Reporte de rango de edades por país" whiteBg icon="ion-ios-podium" desc="Este reporte muestra una gráfica de barras del rango de edades (de diez en diez) por cada país.">
        <div>
          <SelectCountry />
        </div>
        <div>
          <RealReporte6 />
        </div>
      </PapperBlock>
    </div>

  );
}
Reporte6.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Reporte6);
