import React from 'react';
import { Helmet } from 'react-helmet';
import brand from 'dan-api/dummy/brand';
import { PapperBlock } from 'dan-components';
import Map from './../../../components/MyComponents/Map';

import {
  Grid
} from '@material-ui/core';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import './style.css';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: '1%',
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  cardAnimation: {
    'animation': 'appear 500ms ease-out forwards'
  },
}));

const styles = theme => ({
  table: {
    '& > div': {
      overflow: 'auto'
    },
    '& table': {
      '& td': {
        wordBreak: 'keep-all'
      },
      [theme.breakpoints.down('md')]: {
        '& td': {
          height: 60,
          overflow: 'hidden',
          textOverflow: 'ellipsis'
        }
      }
    }
  },
  redBtn: {
    color: '#f44336',
    'border-color': '#f44336'
  },
  blueBtn: {
    color: '#7986cb',
    'border-color': '#7986cb'
  },
  blackBtn: {
    color: '#000000',
    'border-color': '#000000'
  },
  appBar: {
    position: 'relative',
  },
  flex: {
    flex: 1,
  },
});

function cambioTitulo(){
  //const demoId = document.querySelector('.MuiTypography-h4');
  //demoId.textContent = 'Podio de paises más vacunados';
  const demoIdC = document.querySelector('.MuiTypography-body1');
  demoIdC.textContent = 'Podio de paises más vacunados';

}

function BlankPage(props) {
  const title = brand.name + ' - Blank Page';
  const description = brand.desc;

  const { classes } = props;
  const classes_ = useStyles();

  //cambioTitulo();

  return (
    <div>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
      </Helmet>
      {/*<PapperBlock title="Blank Page" desc="Some text description">
        Content
      </PapperBlock>
      <input type="text" id="paises"/>*/}
      <Grid container spacing={3}>
      <Grid item xs={12} sm={6} >
      </Grid>
        <Grid item xs={12} sm={12} >
          <Card className={classes.root} id="glass">
            <Map/>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
}
//className="cardAnimation"
BlankPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BlankPage);
