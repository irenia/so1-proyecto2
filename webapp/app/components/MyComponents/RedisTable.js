import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { lambda } from './../../services/lambda';
import axios from 'axios';
import { makeStyles } from '@material-ui/core/styles';
import { PapperBlock } from 'dan-components';
import './MongoTable.css';


import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

const styles_ = {
    appBar: {
        position: 'relative',
    },
    flex: {
        flex: 1,
    },
};

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        paddingLeft: '5%',
        paddingRight: '5%',
        paddingTop: '1%'
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    izq: {
        'float': 'right'
    },
    cardAnimation: {
        'animation': 'appear 500ms ease-out forwards'
    },
}));

const styles = theme => ({
    table: {
        '& > div': {
            overflow: 'auto'
        },
        '& table': {
            '& td': {
                wordBreak: 'keep-all'
            },
            [theme.breakpoints.down('md')]: {
                '& td': {
                    height: 60,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis'
                }
            }
        }
    },
    button: {
        margin: theme.spacing(0),
    },
    redBtn: {
        color: '#f44336',
        'border-color': '#f44336'
    },
    blueBtn: {
        color: '#7986cb',
        'border-color': '#7986cb'
    },
    blackBtn: {
        color: '#000000',
        'border-color': '#000000'
    },
    appBar: {
        position: 'relative',
    },
});

function AdvFilter(props) {

    const [msg, setMsg] = useState([]);

    const getMsg = async () => {
        try {
            let url = lambda + "/lifo";
            await axios.get(url).then(data => {
                //console.log(data);
                const result = [];
                var contador = 0;
                data.data.forEach(item => {
                    result[contador] = [item.name, item.location, item.gender, item.age + " años", item.vaccine_type, item.way];
                    contador++;
                });
                setMsg(result);
            });
        }
        catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {
        const timeOut = setInterval(() => {
            getMsg();
        }, 1000)

        getMsg();

        return () => {
            clearInterval(timeOut);
        }
    }, [])


    const columns = [
        {
            name: 'Nombre',
            options: {
                filter: true,

            }
        },
        {
            name: 'País',
            options: {
                filter: true
            }
        },
        {
            name: 'Género',
            options: {
                filter: true
            }
        },
        {
            name: 'Edad',
            options: {
                filter: true
            }
        },
        {
            name: 'Tipo de vacuna',
            options: {
                filter: true
            }
        },
        {
            name: 'Camino',
            options: {
                filter: true
            }
        },
    ];

    const options = {
        filterType: 'dropdown',
        responsive: 'stacked',
        fixedHeader: true,
        elevation: 0,
        rowsPerPage: 10,
        page: 0,
        selectableRows: false
    };

    function d(asd) {
        var newArray = [];
        for (var i = 0; i < asd.length; i++) {
            if (asd[i] === undefined) {

            } else {
                newArray.push(asd[i]);
            }
        }
        //console.log(newArray);
        return newArray;
    }

    const { classes } = props;
    const classes_ = useStyles();
    //const { open, scroll } = state;

    return (
        <div className={classes.table} id="prueba">
            <PapperBlock title="Datos almacenados en RedisDB" whiteBg icon="ion-ios-cloud-done" desc="Listado de todas las personas vacunadas" id="encabezado">
                <MuiThemeProvider theme={myTheme}>
                    <MUIDataTable className="mitabla"
                        data={d(msg)}
                        columns={columns}
                        options={options}
                    />
                </MuiThemeProvider>
            </PapperBlock>
        </div>
    );
}

AdvFilter.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AdvFilter);

const myTheme = createMuiTheme({
    overrides: {
        MUIDataTable: {
            responsiveScroll: {
                maxHeight: "580px"
                // overflowY: 'scroll',
            }
        }
    }
});