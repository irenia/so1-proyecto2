import React, { Component, useEffect, useState } from 'react';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import { lambda } from './../../services/lambda';
import axios from 'axios';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import "./Reporte6.css";
const url = lambda + "/rango";

class FinalFootGraph extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: [1, 2, 3],
        };
    }



    componentDidMount() {
        /* Chart code */
        // Themes begin
        //am4core.useTheme(am4themes_animated);
        // Themes end

        this.intervalID = setInterval(async () => {
            if (localStorage.getItem('paises')) {
                //console.log(JSON.stringify(localStorage.getItem('paises')))
                var x = document.getElementById("chartdiv_");
                x.style.display = "block";
                await axios.get(url).then(data => {
                    let chart = am4core.create("chartdiv_", am4charts.XYChart);
                    chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
                    let result = [];
                    var contador = 0;
                    var contadorCeroDiez = 0;
                    data.data.forEach(item => {
                        console.log(item);
                        var rompePaises = localStorage.getItem('paises').split(",");
                        for (var j = 0; j < rompePaises.length; j++) {
                            if (rompePaises[j] == item.pais) {
                                if (item.r10 > 0) {
                                    result[contador] = { country: item.pais + " (0-10)", visits: item.r10 };
                                    contador++;
                                }
                                if (item.r20 > 0) {
                                    result[contador] = { country: item.pais + " (10-20)", visits: item.r20 };
                                    contador++;
                                }
                                if (item.r30 > 0) {
                                    result[contador] = { country: item.pais + " (20-30)", visits: item.r30 };
                                    contador++;
                                }
                                if (item.r40 > 0) {
                                    result[contador] = { country: item.pais + " (40-50)", visits: item.r40 };
                                    contador++;
                                }
                                if (item.r50 > 0) {
                                    result[contador] = { country: item.pais + " (50-60)", visits: item.r50 };
                                    contador++;
                                }
                                if (item.r60 > 0) {
                                    result[contador] = { country: item.pais + " (60-70)", visits: item.r60 };
                                    contador++;
                                }
                                if (item.r70 > 0) {
                                    result[contador] = { country: item.pais + " (70-80)", visits: item.r70 };
                                    contador++;
                                }
                                if (item.r80 > 0) {
                                    result[contador] = { country: item.pais + " (80-90)", visits: item.r80 };
                                    contador++;
                                }
                                if (item.r90 > 0) {
                                    result[contador] = { country: item.pais + " (90-100)", visits: item.r90 };
                                    contador++;
                                }
                                break;
                            }
                        }
                    })

                    //chart.data.length = 0;
                    //chart.data[0] = {country: "Guatemala 0-10",visits: 23725};
                    /*this.setState({
                        list: [
                            { "country": result[0].country, "visits": result[0].visits }
                        ]
                    });*/
                    chart.data = result;
                    //chart.invalidateData();
                    //console.log(chart.data);

                    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.renderer.grid.template.location = 0;
                    categoryAxis.dataFields.category = "country";
                    categoryAxis.renderer.minGridDistance = 40;
                    categoryAxis.fontSize = 11;

                    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
                    valueAxis.min = 0;
                    valueAxis.max = 100;
                    valueAxis.strictMinMax = true;
                    valueAxis.renderer.minGridDistance = 30;
                    // axis break
                    let axisBreak = valueAxis.axisBreaks.create();
                    axisBreak.startValue = 40;
                    axisBreak.endValue = 98;
                    //axisBreak.breakSize = 0.005;

                    // fixed axis break
                    let d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
                    axisBreak.breakSize = 0.05 * (1 - d) / d; // 0.05 means that the break will take 5% of the total value axis height

                    // make break expand on hover
                    let hoverState = axisBreak.states.create("hover");
                    hoverState.properties.breakSize = 1;
                    hoverState.properties.opacity = 0.1;
                    //hoverState.transitionDuration = 1500;

                    axisBreak.defaultState.transitionDuration = 1000;
                    /*
                    // this is exactly the same, but with events
                    axisBreak.events.on("over", function() {
                      axisBreak.animate(
                        [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
                        1500,
                        am4core.ease.sinOut
                      );
                    });
                    axisBreak.events.on("out", function() {
                      axisBreak.animate(
                        [{ property: "breakSize", to: 0.005 }, { property: "opacity", to: 1 }],
                        1000,
                        am4core.ease.quadOut
                      );
                    });*/

                    let series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.categoryX = "country";
                    series.dataFields.valueY = "visits";
                    series.columns.template.tooltipText = "{valueY.value}";
                    series.columns.template.tooltipY = 0;
                    series.columns.template.strokeOpacity = 0;

                    // as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
                    series.columns.template.adapter.add("fill", function (fill, target) {
                        return chart.colors.getIndex(target.dataItem.index);
                    });

                });
            } else {
                /*if (this.chart) {
                    this.chart.dispose();
                }*/
                var x = document.getElementById("chartdiv_");
                if(x){
                    x.style.display = "none";
                }
            }
        }, 2500);
    }

    componentWillUnmount() {
        if (this.chart_) {
            this.chart_.dispose();
        }
    }

    render() {
        return (
            <div className="mapChart">
                <div className="stats" style={{
                    textAlign: 'center'
                }}>
                    <h6 className="mt-1">Gráfica de barras del rango de edades por país de personas vacunadas</h6>
                    <p className="h3 m-0">
                        <span className="mr-xs fw-normal">
                        </span>
                        <i className="fa fa-map-marker" />
                    </p>
                </div>
                <div className="bar" id="chartdiv_" style={{
                    height: '350px'
                }}>
                </div>
            </div>
        );
    }
}

export default FinalFootGraph;