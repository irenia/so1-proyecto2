module.exports = {
  name: 'COVID vacunación',
  desc: 'COVID-19 Realtime Vaccinated People Visualizer',
  prefix: 'COVID-19_Realtime_Vaccinated',
  footerText: 'COVID-19 Realtime Vaccinated People Visualizer Pro All Rights Reserved 2018',
  logoText: 'COVID-19 Realtime Vaccinated',
};


// COVID-19 Realtime Vaccinated People Visualizer